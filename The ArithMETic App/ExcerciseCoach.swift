//
//  ExcerciseCoach.swift
//  The ArithMETic App
//
//  Created by Student on 2/14/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import Foundation

struct ExcerciseCoach {
    
    static let sportsActivity:[String:Double] = ["Bicycling":
        8.0,"Jumping rope":12.3, "Running - slow":
        9.8,"Running - fast":
        23.0,"Tennis":
        8.0,"Swimming":5.8]
    
    func EnergyConsumed(during:String, weight:Double, time:Int) -> Double{
        let energyconsumed:Double
        var metValue:Double
        switch during {
        case "Tennis":
            metValue = ExcerciseCoach.sportsActivity["Tennis"]!; break
        case "Jumping rope":
            metValue = ExcerciseCoach.sportsActivity["Jumping rope"]!; break
        case "Running - slow":
            metValue = ExcerciseCoach.sportsActivity["Running - slow"]!; break
        case "Bicycling":
            metValue = ExcerciseCoach.sportsActivity["Bicycling"]!; break
        case "Swimming":
            metValue = ExcerciseCoach.sportsActivity["Swimming"]!; break
        case "Running - fast":
            metValue = ExcerciseCoach.sportsActivity["Running - fast"]!; break
        default:
            metValue = 0
        }
        energyconsumed = metValue * 3.5 * (Double(weight )/2.2) / 200
        let energyConsumedinTime = Double(energyconsumed * Double(time))
        return energyConsumedinTime
    }
    func TimeToLose1Pound(during:String, weight:Double) -> Double{
        let energyconsumed:Double
        var metValue:Double
        switch during {
        case "Tennis":
            metValue = ExcerciseCoach.sportsActivity["Tennis"]!; break
        case "Jumping rope":
            metValue = ExcerciseCoach.sportsActivity["Jumping rope"]!; break
        case "Running - slow":
            metValue = ExcerciseCoach.sportsActivity["Running - slow"]!; break
        case "Bicycling":
            metValue = ExcerciseCoach.sportsActivity["Bicycling"]!; break
        case "Swimming":
            metValue = ExcerciseCoach.sportsActivity["Swimming"]!; break
        case "Running - fast":
            metValue = ExcerciseCoach.sportsActivity["Running - fast"]!; break
        default:
            metValue = 0
        }
        energyconsumed = metValue * 3.5 * (Double(weight )/2.2) / 200
        
        let losein1pound = Double(3500/energyconsumed)
        return losein1pound
        
    }
    
}
