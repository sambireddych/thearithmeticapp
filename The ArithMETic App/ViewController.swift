//
//  ViewController.swift
//  The ArithMETic App
//
//  Created by Student on 2/14/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var losein1pound: UILabel!
    @IBOutlet weak var EcLBL: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var EtTF: UITextField!
    @IBOutlet weak var weightTF: UITextField!
    var pickerData:[String:Double] = [:]
    var activities:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.view.backgroundColor = UIColor.rg
        
        pickerData = ExcerciseCoach.sportsActivity
        self.picker.delegate = self
        self.picker.dataSource = self
        activities = Array(pickerData.keys)
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return activities.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //        let dummy = Array(pickerData.keys)
        return activities[row]
    }
    
    
    
    @IBAction func CalculateBN(_ sender: Any) {
        
        let energyconsumedResult = ExcerciseCoach.init().EnergyConsumed(during: activities[picker.selectedRow(inComponent: 0)]
            , weight: Double(weightTF.text!) ?? 0, time: Int(EtTF.text!) ?? 0)
        let losein1poundResult = String(format: "Time to lose 1 pound: %.1f minutes", ExcerciseCoach.init().TimeToLose1Pound(during: activities[picker.selectedRow(inComponent: 0)]
            , weight: Double(weightTF.text!) ?? 0))
        EcLBL.text = String(format: "Energy Consumed:   %.1f cal", energyconsumedResult)
        losein1pound.text = losein1poundResult
    }
    
    @IBAction func ClearBN(_ sender: Any) {
        EtTF.text! = ""
        weightTF.text! = ""
        EcLBL.text = "Energy Consumed: 0 cal"
        losein1pound.text = "Time to lose 1 pound: 0 minutes"
    }

}

